# -*- coding: utf-8 -*-
"""ML_Assignment_Three_Q2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1zfJg_46qSQwhcyjtUHiGvCIWEH9QpSyN

Import required libraries
"""

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from keras.optimizers import RMSprop
from keras.datasets import mnist
from keras.utils import np_utils
import matplotlib.pyplot as plt
import numpy as np

"""Load the data"""

# Load MNIST dataset
(x_train_original, y_train_original), (x_test_original, y_test_original) = mnist.load_data()

"""Build the model"""

# Convolutional model
model = Sequential()

# First convolution layer
model.add(Conv2D(32, kernel_size=(3,3), strides=(2, 2), padding="same", activation='relu', input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2), padding="same"))
model.add(Dropout(0.2))

# Second convolution layer
model.add(Conv2D(64, kernel_size=(3,3), strides=(2, 2), padding="same", activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2), padding="same"))
model.add(Dropout(0.2))

# Third convolution layer with flattern
model.add(Conv2D(128, kernel_size=(3,3), strides=(2, 2), padding="same", activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2,2), padding="same"))
model.add(Flatten())
model.add(Dropout(0.2))

# Fully connected layer one
model.add(Dense(625, activation='relu'))
model.add(Dropout(0.5))

# Fully connected layer two
model.add(Dense(10, activation='softmax'))

"""Scenario one

Add noise before do the preprocess data
"""

# Add noise
noise_factor = 0.25
x_train_noisy = x_train_original + noise_factor * np.random.normal(loc = 0.0, scale = 1.0, size = x_train_original.shape)
x_test_noisy = x_test_original + noise_factor * np.random.normal(loc = 0.0, scale = 1.0, size = x_test_original.shape)

x_train_noisy = np.clip(x_train_noisy, 0, 1)
x_test_noisy = np.clip(x_test_noisy, 0, 1)

# Reshaping the array to 4-dimsions
x_train_noisy = x_train_noisy.reshape(x_train_noisy.shape[0], 28, 28, 1)
x_test_noisy = x_test_noisy.reshape(x_test_noisy.shape[0], 28, 28, 1)
input_shape = (28, 28, 1)

# Normalizing the RGB codes by dividing it to the max RGB value.
x_train_noisy = x_train_noisy.astype('float32') / 255
x_test_noisy = x_test_noisy.astype('float32') / 255

# Create categorical class label data for loss function
y_train = np_utils.to_categorical(y_train_original, 10)
y_test = np_utils.to_categorical(y_test_original, 10)

print('x_train_noisy shape:', x_train_noisy.shape)
print('Number of images in x_train_noisy', x_train_noisy.shape[0])
print('Number of images in x_test_noisy', x_test_noisy.shape[0])

"""Add given noise factor before preprodess the data and evaluate the model"""

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
history = model.fit(x_train_noisy, y_train, epochs=10, batch_size=128, shuffle=True, verbose=1)
evaluation = model.evaluate(x_test_noisy, y_test, batch_size=256, verbose=1)

print("\n\n")
print('Evalution summary: Loss over the test dataset : %.5f, Accuracy : %.5f' % (evaluation[0], evaluation[1]))

"""Accuracy and loss values for various noise values"""

# Check accuracy and loss values agains different noise factors
noise_factors = [0.01, 0.02, 0.025, 0.1, 0.2, 0.25, 0.3, 0.35]
accuracy = []
loss = []

for noise in noise_factors:

  x_train_noisy = x_train_original + noise * np.random.normal(loc = 0.0, scale = 1.0, size = x_train_original.shape)
  x_test_noisy = x_test_original + noise * np.random.normal(loc = 0.0, scale = 1.0, size = x_test_original.shape)

  x_train_noisy = np.clip(x_train_noisy, 0, 1)
  x_test_noisy = np.clip(x_test_noisy, 0, 1)

  # Reshaping the array to 4-dimsions
  x_train_noisy = x_train_noisy.reshape(x_train_noisy.shape[0], 28, 28, 1)
  x_test_noisy = x_test_noisy.reshape(x_test_noisy.shape[0], 28, 28, 1)
  input_shape = (28, 28, 1)

  # Normalizing the RGB codes by dividing it to the max RGB value.
  x_train_noisy = x_train_noisy.astype('float32') / 255
  x_test_noisy = x_test_noisy.astype('float32') / 255

  # Evaluation
  model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
  history = model.fit(x_train_noisy, y_train, epochs=10, batch_size=128, shuffle=True, verbose=1)
  evaluation = model.evaluate(x_test_noisy, y_test, batch_size=256, verbose=1)
  
  loss.append(evaluation[0])
  accuracy.append(evaluation[1])

"""Accuracy data table and loss data table"""

# Show previos evaluate values
index_1 = 0
for value in accuracy:
  print('Noise factor : %.4f, Accuracy : %.5f' % (noise_factors[index_1], value))
  index_1 = index_1 + 1


index_2 = 0
for value in loss:
  print('Noise factor : %.4f, Loss : %.5f' % (noise_factors[index_2], value))
  index_2 = index_2 + 1

"""Plot result data"""

# Plot the evaluate results
plt.title('Accuracy against Number of noise factor')
plt.plot(noise_factors, accuracy, color='blue', linewidth=2 , label = "Accuracy")
plt.legend()
plt.show()

print("\n\n")

plt.title('Loss against Number of noise factor')
plt.plot(noise_factors, loss, color='blue', linewidth=2, label = "Loss")
plt.legend()
plt.show()

"""Scenario two

Add noise after the preprocess data
"""

# Reshaping the array to 4-dimsions
x_train = x_train_original.reshape(x_train_original.shape[0], 28, 28, 1)
x_test = x_test_original.reshape(x_test_original.shape[0], 28, 28, 1)
input_shape = (28, 28, 1)

# Normalizing the RGB codes by dividing it to the max RGB value.
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

# Add noise
noise_factor = 0.25
x_train_noisy = x_train + noise_factor * np.random.normal(loc = 0.0, scale = 1.0, size = x_train.shape)
x_test_noisy = x_test + noise_factor * np.random.normal(loc = 0.0, scale = 1.0, size = x_test.shape)

x_train = np.clip(x_train, 0, 1)
x_test = np.clip(x_test, 0, 1)

# Create categorical class label data for loss function
y_train = np_utils.to_categorical(y_train_original, 10)
y_test = np_utils.to_categorical(y_test_original, 10)

print('x_train_noisy shape:', x_train_noisy.shape)
print('Number of images in x_train_noisy', x_train_noisy.shape[0])
print('Number of images in x_test_noisy', x_test_noisy.shape[0])

"""Add noise after the preprocess and evaluate the model"""

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
history = model.fit(x_train_noisy, y_train, epochs=10, batch_size=128, shuffle=True, verbose=1)
evaluation = model.evaluate(x_test_noisy, y_test, batch_size=256, verbose=1)

print("\n\n")
print('Evalution summary: Loss over the test dataset : %.5f, Accuracy : %.5f' % (evaluation[0], evaluation[1]))

"""Accuracy and loss values for various noise values"""

# Check accuracy and loss values agains different noise factors
noise_factors1 = [0.01, 0.02, 0.025, 0.1, 0.2, 0.25, 0.3, 0.35]
accuracy1 = []
loss1 = []

for noise in noise_factors1:

  x_train_noisy = x_train + noise * np.random.normal(loc = 0.0, scale = 1.0, size = x_train.shape)
  x_test_noisy = x_test + noise * np.random.normal(loc = 0.0, scale = 1.0, size = x_test.shape)

  x_train_noisy = np.clip(x_train_noisy, 0, 1)
  x_test_noisy = np.clip(x_test_noisy, 0, 1)

  # Evaluation
  model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
  history = model.fit(x_train_noisy, y_train, epochs=10, batch_size=128, shuffle=True, verbose=1)
  evaluation = model.evaluate(x_test_noisy, y_test, batch_size=256, verbose=1)
  
  loss1.append(evaluation[0])
  accuracy1.append(evaluation[1])

"""Accuracy data table and loss data table"""

# Show previos evaluate values
index_3 = 0
for value in accuracy1:
  print('Noise factor : %.4f, Accuracy : %.5f' % (noise_factors1[index_3], value))
  index_3 = index_3 + 1


index_4 = 0
for value in loss1:
  print('Noise factor : %.4f, Loss : %.5f' % (noise_factors1[index_4], value))
  index_4 = index_4 + 1

"""Plot result data"""

# Plot the evaluate results
plt.title('Accuracy against Number of noise factor')
plt.plot(noise_factors1, accuracy1, color='blue', linewidth=2, label = "Accuracy")
plt.legend()
plt.show()

print("\n\n")

plt.title('Lost against Number of noise factor')
plt.plot(noise_factors1, loss1, color='blue', linewidth=2, label = "Loss")
plt.legend()
plt.show()